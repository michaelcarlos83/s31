let http = require('http')

let port = 4000

let server = http.createServer((request, response) => {
	if(request.url == '/login'){
	response.writeHead(200, {'Content-Type':'text/plain'})
	
	response.end("Welcome to the login page!")
	} else if(request.url == '/') {
	response.writeHead(200, {'Content-Type':'text/plain'})

	response.end("server is successfully running")
	} else {
    response.writeHead(404, {'Content-Type':'text/plain'})
	
	response.end("Page not found")
	}
})

server.listen(port)
console.log(`Server is running at localhost:${port}`)

